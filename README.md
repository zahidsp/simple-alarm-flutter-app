# Simple Alarm App

This project is a [Flutter](https://flutter.dev) application.<br/>
Simple alarm app is an app where the user can set the time by rotating the clock needles.
<br/><br/>

## Prerequisites

an IDE, you can use:

- [Android Studio](https://developer.android.com/studio) or
- [VScode IDE](https://code.visualstudio.com/)

Follow the [documentation](https://flutter.dev/docs/get-started/install) for flutter installation.<br/><br/>

## Installation

1. Clone this repository.<br/>
2. Run flutter get packages.<br/>

```shell
 flutter pub get
```

<br/><br/>

## Run Project

1. Connect a device or use android emulator or use iOS simulator.<br/>
2. Make sure your device has been attached correctly.

<br/>
For check android device attached run:<br/>

```shell
 adb devices

List of devices attached
JCAXGF00P731RDE	device
```

3. Now run the project.<br/>

```shell
 flutter run
```

4. in VS Code, you can run from <mark>Run<mark> menu and choose the run option<br/>

## Clean Run

The clean run procedure is needed when a non-technical error happened. This error commonly occurs was caused by the latest build couldn't renew the old build of the app. The clean run procedure can be done on some steps as follow:

1. Delete all caches and old build app of the project.<br/>

```shell
 flutter clean
```

2. Run flutter get packages.<br/>

```shell
 flutter pub get
```

3. Uninstall the app in the emulator/smartphone.<br/>
4. Re-run the project.<br/><br/>
