import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:simple_alarm_app/data/constant.dart';
import 'package:simple_alarm_app/util/utils.dart';

Future<void> initializeNotification() async {
  AwesomeNotifications().initialize(
    null,
    [
      NotificationChannel(
        channelKey: Constant.scheduledChannelKey,
        channelName: Constant.scheduledChannelValue,
        channelDescription: Constant.scheduledChannelDesc,
        defaultColor: Constant.primaryColor,
        locked: true,
        importance: NotificationImportance.High,
      ),
    ],
  );
}

Future<void> createOpenReminderNotification(
    int hour, int minute, bool isAm) async {
  await AwesomeNotifications().createNotification(
    content: NotificationContent(
      id: createUniqueId(),
      channelKey: Constant.scheduledChannelKey,
      title: '((( Ringing )))',
      body: 'Hello there!',
      notificationLayout: NotificationLayout.Default,
    ),
    schedule: NotificationCalendar(
      hour: isAm ? hour : hour + 12,
      minute: minute,
      second: 0,
      millisecond: 0,
      repeats: true,
    ),
  );
}

Future<void> cancelScheduledNotifications() async {
  await AwesomeNotifications().cancelAllSchedules();
}
