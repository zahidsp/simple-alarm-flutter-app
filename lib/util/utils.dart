int createUniqueId() {
  return DateTime.now().millisecondsSinceEpoch.remainder(100000);
}

DateTime setDateTime(bool isAm, String hour, String minute) {
  DateTime now = DateTime.now();
  return DateTime(
      now.year,
      now.month,
      now.hour > (isAm ? int.parse(hour) : int.parse(hour) + 12)
          ? now.day + 1
          : now.hour == (isAm ? int.parse(hour) : int.parse(hour) + 12) &&
                  now.minute >= int.parse(minute) &&
                  now.second > 0
              ? now.day + 1
              : now.day,
      isAm ? int.parse(hour) : int.parse(hour) + 12,
      int.parse(minute));
}
