import 'package:flutter/material.dart';

class Constant {
  static const primaryColor = Color(0xFF1CE6C4);

  static const String scheduledChannelKey = 'scheduled_channel';
  static const String scheduledChannelValue = 'Scheduled Notifications';
  static const String scheduledChannelDesc = 'Scheduled Notifications Desc';
}
