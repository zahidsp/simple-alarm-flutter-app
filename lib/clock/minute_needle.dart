import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/minute_bloc/minute_bloc.dart';
import '../data/constant.dart';

class MinuteNeedle extends StatefulWidget {
  const MinuteNeedle({Key? key}) : super(key: key);

  @override
  _MinuteNeedleState createState() => _MinuteNeedleState();
}

class _MinuteNeedleState extends State<MinuteNeedle>
    with SingleTickerProviderStateMixin {
  double degree = 0;
  int currentVal = 0;
  late double clockSize;
  late double radius;
  late AnimationController minuteAnimationCtrl;

  @override
  void initState() {
    super.initState();
    degree = 0;
    clockSize = 300;
    radius = clockSize / 2;
    minuteAnimationCtrl = AnimationController.unbounded(vsync: this);
    minuteAnimationCtrl.value = degree;
  }

  double roundDegree(double degree) {
    late int x;
    x = int.tryParse(degree.toString().split('.')[0])!;
    return x.toDouble();
  }

  _onPanUpdate(DragUpdateDetails drag) {
    bool onTop = drag.localPosition.dy <= radius;
    bool onLeftSide = drag.localPosition.dx <= radius;
    bool onRightSide = !onLeftSide;
    bool onBottom = !onTop;

    bool dragUp = drag.delta.dy <= 0.0;
    bool dragLeft = drag.delta.dx <= 0.0;
    bool dragRight = !dragLeft;
    bool dragDown = !dragUp;

    double yChange = drag.delta.dy.abs();
    double xChange = drag.delta.dx.abs();

    double verticalRotation =
        (onRightSide && dragDown) || (onLeftSide && dragUp)
            ? yChange
            : yChange * -1;

    double horizontalRotation =
        (onTop && dragRight) || (onBottom && dragLeft) ? xChange : xChange * -1;

    double rotationalChange = verticalRotation + horizontalRotation;

    double _value = degree + (rotationalChange / 5);

    setState(() {
      degree = _value > 0 ? _value : 0;
      minuteAnimationCtrl.value = degree;
      var lap = degree > 360 ? degree / 360 : 1;
      var a = degree < 360
          ? degree.roundToDouble()
          : degree - (roundDegree(lap.toDouble()) * 360);

      var degrees = a.roundToDouble();
      currentVal = degrees ~/ 6;
      BlocProvider.of<MinuteBloc>(context).add(SetMinEvent(minute: currentVal));
    });
  }

  @override
  Widget build(BuildContext context) {
    GestureDetector draggableMinute = GestureDetector(
      onPanUpdate: _onPanUpdate,
      child: Container(
        height: radius * 2,
        width: radius * 2,
        decoration: const BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),
        child: Align(
          alignment: const Alignment(0, 0),
          child: AnimatedBuilder(
            animation: minuteAnimationCtrl,
            builder: (context, widget) {
              return Transform.rotate(
                angle: -pi / 2,
                child: CustomPaint(
                  painter: MinNeedlePainter(degrees: minuteAnimationCtrl.value),
                ),
              );
            },
          ),
        ),
      ),
    );
    return draggableMinute;
  }
}

class MinNeedlePainter extends CustomPainter {
  double degrees;
  MinNeedlePainter({
    required this.degrees,
  });
  @override
  void paint(Canvas canvas, Size size) {
    var centerX = size.width / 2;
    var centerY = size.height / 2;
    var center = Offset(centerX, centerY);

    var minHandBrush = Paint()
      ..color = Constant.primaryColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 12
      ..strokeCap = StrokeCap.round;

    var minHandX = centerX + 96 * cos(degrees * pi / 180);
    var minHandY = centerY + 96 * sin(degrees * pi / 180);
    canvas.drawLine(center, Offset(minHandX, minHandY), minHandBrush);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
