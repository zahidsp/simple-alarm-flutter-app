import 'package:flutter/material.dart';
import 'package:simple_alarm_app/clock/hour_needle.dart';
import 'clock_base.dart';
import 'minute_needle.dart';

class ClockView extends StatefulWidget {
  const ClockView({Key? key}) : super(key: key);

  @override
  _ClockViewState createState() => _ClockViewState();
}

class _ClockViewState extends State<ClockView> with TickerProviderStateMixin {
  double clockSize = 300;

  @override
  Widget build(BuildContext context) {
    ClockBase clockBase = ClockBase(context: context);

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ),
        Stack(
          alignment: Alignment.center,
          children: [
            SizedBox(
              width: clockSize,
              height: clockSize,
              child: Transform.rotate(
                  angle: 2,
                  child: Center(child: CustomPaint(painter: clockBase))),
            ),
            const MinuteNeedle(),
            const HourNeedle(),
          ],
        )
      ],
    );
  }
}
