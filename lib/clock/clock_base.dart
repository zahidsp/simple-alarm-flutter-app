import 'dart:math';

import 'package:flutter/material.dart';
import 'package:simple_alarm_app/data/constant.dart';

class ClockBase extends CustomPainter {
  double radius = 0;
  BuildContext context;

  ClockBase({required this.context}) {
    radius = 150;
  }

  @override
  void paint(Canvas canvas, Size size) {
    var centerX = size.width / 2;
    var centerY = size.height / 2;
    var center = Offset(centerX, centerY);

    final linePaint = Paint()
      ..strokeCap = StrokeCap.round
      ..color = Colors.white
      ..strokeWidth = 4;

    var baseClockBrush = Paint()
      ..color = Constant.primaryColor.withOpacity(0.1);
    final centerClockBrush = Paint()..color = Colors.white24;

    canvas.drawCircle(center, radius - 32, baseClockBrush);

    var innerCircleRadius = radius - 16;
    for (double i = 0; i < 360; i += 12) {
      var x1 = centerX + radius * cos(i * pi / 180);
      var y1 = centerX + radius * sin(i * pi / 180);

      var x2 = centerX + innerCircleRadius * cos(i * pi / 180);
      var y2 = centerX + innerCircleRadius * sin(i * pi / 180);
      canvas.drawLine(Offset(x1, y1), Offset(x2, y2), linePaint);
    }
    canvas.drawCircle(center, 16, centerClockBrush);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
