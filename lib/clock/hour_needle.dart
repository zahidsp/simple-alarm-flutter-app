import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/hour_bloc/hour_bloc.dart';

class HourNeedle extends StatefulWidget {
  const HourNeedle({Key? key}) : super(key: key);

  @override
  _HourNeedleState createState() => _HourNeedleState();
}

class _HourNeedleState extends State<HourNeedle>
    with SingleTickerProviderStateMixin {
  double degree = 0;
  int currentVal = 0;
  late double clockSize;
  late double radius;
  late AnimationController hourAnimationCtrl;

  @override
  void initState() {
    super.initState();
    degree = 0;
    clockSize = 300;
    radius = clockSize / 2;
    hourAnimationCtrl = AnimationController.unbounded(vsync: this);
    hourAnimationCtrl.value = degree;
  }

  double roundDegree(double degree) {
    late int x;
    x = int.tryParse(degree.toString().split('.')[0])!;
    return x.toDouble();
  }

  _onPanUpdate(DragUpdateDetails drag) {
    bool onTop = drag.localPosition.dy <= radius;
    bool onLeftSide = drag.localPosition.dx <= radius;
    bool onRightSide = !onLeftSide;
    bool onBottom = !onTop;

    bool dragUp = drag.delta.dy <= 0.0;
    bool dragLeft = drag.delta.dx <= 0.0;
    bool dragRight = !dragLeft;
    bool dragDown = !dragUp;

    double yChange = drag.delta.dy.abs();
    double xChange = drag.delta.dx.abs();

    double verticalRotation =
        (onRightSide && dragDown) || (onLeftSide && dragUp)
            ? yChange
            : yChange * -1;

    double horizontalRotation =
        (onTop && dragRight) || (onBottom && dragLeft) ? xChange : xChange * -1;

    double rotationalChange = verticalRotation + horizontalRotation;

    double _value = degree + (rotationalChange / 5);

    setState(() {
      degree = _value > 0 ? _value : 0;
      hourAnimationCtrl.value = degree;
      var lap = degree > 360 ? degree / 360 : 1;
      var a = degree < 360
          ? degree.roundToDouble()
          : degree - (roundDegree(lap.toDouble()) * 360);
      var degrees = a.roundToDouble();
      currentVal = degrees ~/ 30;

      BlocProvider.of<HourBloc>(context).add(SetHourEvent(hour: currentVal));
    });
  }

  @override
  Widget build(BuildContext context) {
    GestureDetector draggablehour = GestureDetector(
      onPanUpdate: _onPanUpdate,
      child: Container(
        height: radius * 1.5,
        width: radius * 1.5,
        decoration: const BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),
        child: Align(
          alignment: const Alignment(0, 0),
          child: AnimatedBuilder(
            animation: hourAnimationCtrl,
            builder: (context, widget) {
              return Transform.rotate(
                angle: -pi / 2,
                child: CustomPaint(
                  painter: HourNeedlePainter(degrees: hourAnimationCtrl.value),
                ),
              );
            },
          ),
        ),
      ),
    );
    return draggablehour;
  }
}

class HourNeedlePainter extends CustomPainter {
  double degrees;
  HourNeedlePainter({
    required this.degrees,
  });
  @override
  void paint(Canvas canvas, Size size) {
    var centerX = size.width / 1.2;
    var centerY = size.height / 1.2;
    var center = Offset(centerX, centerY);

    var minHandBrush = Paint()
      ..color = Colors.grey
      ..style = PaintingStyle.stroke
      ..strokeWidth = 12
      ..strokeCap = StrokeCap.round;

    var minHandX = centerX + 54 * cos(degrees * pi / 180);
    var minHandY = centerY + 54 * sin(degrees * pi / 180);
    canvas.drawLine(center, Offset(minHandX, minHandY), minHandBrush);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
