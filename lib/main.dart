import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/hour_bloc/hour_bloc.dart';
import 'bloc/minute_bloc/minute_bloc.dart';
import 'data/constant.dart';
import 'home_screen.dart';
import 'service/local_notifications.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeNotification();

  runApp(MultiBlocProvider(providers: [
    BlocProvider<MinuteBloc>(
      create: (context) => MinuteBloc(),
    ),
    BlocProvider<HourBloc>(
      create: (context) => HourBloc(),
    ),
  ], child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if (!isAllowed) {
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });

    AwesomeNotifications().actionStream.listen((notification) {
      if (notification.channelKey == Constant.scheduledChannelKey) {
        // TODO:: Route when notif open
      }
    });
  }

  @override
  void dispose() {
    AwesomeNotifications().actionSink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
    );
  }
}
