import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import 'bloc/hour_bloc/hour_bloc.dart';
import 'bloc/minute_bloc/minute_bloc.dart';
import 'clock/clock_view.dart';
import 'data/constant.dart';
import 'service/local_notifications.dart';
import 'util/utils.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache = AudioCache();
  String filePath = 'audio.mp3';

  String hour = '0';
  String minute = '0';
  bool isAm = true;
  List<bool> isSelected = List.generate(2, (index) => false);

  @override
  void initState() {
    super.initState();
    audioCache = AudioCache(fixedPlayer: audioPlayer);
  }

  @override
  void dispose() {
    audioPlayer.release();
    audioPlayer.dispose();
    audioCache.clearAll();
    super.dispose();
  }

  playLocalAsset() async {
    await audioCache.play(filePath);
    Future.delayed(const Duration(seconds: 15), () async {
      await audioPlayer.stop();
    });
  }

  void showSnackBar() {
    String time = DateFormat('dd-MMM-yyy  hh:mm a').format(
      setDateTime(isAm, hour, minute),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Alarm is set for $time'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      alignment: Alignment.center,
      color: Colors.black87,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 72),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BlocBuilder<HourBloc, HourState>(
                  builder: (context, state) {
                    Future.delayed(Duration.zero, () {
                      if (state is DisplayHourState) {
                        setState(() {
                          hour = state.hour;
                        });
                      }
                    });
                    return Text(state is DisplayHourState ? state.hour : '00',
                        style: const TextStyle(
                          fontSize: 54,
                          fontWeight: FontWeight.bold,
                          color: Constant.primaryColor,
                        ));
                  },
                ),
                const SizedBox(width: 5),
                const Text(':',
                    style: TextStyle(
                        fontSize: 54,
                        fontWeight: FontWeight.bold,
                        color: Constant.primaryColor)),
                const SizedBox(width: 5),
                BlocBuilder<MinuteBloc, MinuteState>(
                  builder: (context, state) {
                    Future.delayed(Duration.zero, () {
                      if (state is DisplayMinState) {
                        setState(() {
                          minute = state.minute;
                        });
                      }
                    });
                    return Text(state is DisplayMinState ? state.minute : '00',
                        style: const TextStyle(
                          fontSize: 54,
                          fontWeight: FontWeight.bold,
                          color: Constant.primaryColor,
                        ));
                  },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16),
            child: ToggleButtons(
              selectedColor: Constant.primaryColor,
              color: Colors.white38,
              borderColor: Constant.primaryColor,
              selectedBorderColor: Constant.primaryColor,
              children: const [
                Text(
                  'AM',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'PM',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
              onPressed: (int index) {
                setState(() {
                  for (int i = 0; i < isSelected.length; i++) {
                    if (i == index) {
                      isAm = index == 0 ? true : false;
                      isSelected[i] = true;
                    } else {
                      isSelected[i] = false;
                    }
                  }
                });
              },
              isSelected: isSelected,
            ),
          ),
          const SizedBox(height: 32),
          const ClockView(),
          const SizedBox(height: 32),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                primary: Constant.primaryColor,
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                elevation: 0,
                minimumSize: const Size(120, 30),
              ),
              onPressed: () async {
                createOpenReminderNotification(
                    int.parse(hour), int.parse(minute), isAm);
                showSnackBar();
              },
              child: const Text('Save Alarm')),
        ],
      ),
    ));
  }
}
