import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'hour_event.dart';
part 'hour_state.dart';

class HourBloc extends Bloc<HourEvent, HourState> {
  HourBloc() : super(HourInitial()) {
    on<SetHourEvent>((event, emit) {
      var value = event.hour.toString();
      if (value.length < 2) {
        value = '0' + value;
      }
      emit(DisplayHourState(hour: value));
    });
  }
}
