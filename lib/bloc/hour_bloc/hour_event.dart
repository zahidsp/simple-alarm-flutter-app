part of 'hour_bloc.dart';

@immutable
abstract class HourEvent {}

class SetHourEvent extends HourEvent {
  final int hour;

  SetHourEvent({required this.hour});
  List<Object> get props => [hour];
}
