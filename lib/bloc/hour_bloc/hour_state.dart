part of 'hour_bloc.dart';

@immutable
abstract class HourState {}

class HourInitial extends HourState {}

class DisplayHourState extends HourState {
  final String hour;

  DisplayHourState({required this.hour});
}
