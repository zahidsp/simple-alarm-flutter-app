part of 'minute_bloc.dart';

@immutable
abstract class MinuteState {}

class MinuteInitial extends MinuteState {}

class DisplayMinState extends MinuteState {
  final String minute;

  DisplayMinState({required this.minute});
}
