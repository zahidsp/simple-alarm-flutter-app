part of 'minute_bloc.dart';

@immutable
abstract class MinuteEvent {}

class SetMinEvent extends MinuteEvent {
  final int minute;

  SetMinEvent({required this.minute});

  List<Object> get props => [minute];
}
