import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'minute_event.dart';
part 'minute_state.dart';

class MinuteBloc extends Bloc<MinuteEvent, MinuteState> {
  MinuteBloc() : super(MinuteInitial()) {
    on<SetMinEvent>((event, emit) {
      var value = event.minute.toString();
      if (value.length < 2) {
        value = '0' + value;
      }
      emit(DisplayMinState(minute: value));
    });
  }
}
